const jwt = require("jsonwebtoken");
require("dotenv").config();

const SECRET_KEY = process.env.SECRET;

const decodeJWT = (header) => {
    console.log(new Date(), "Header from User service: ", header);

    const jwtToken = header.split(" ")[1];

    console.log("JWT_TOKEN: ", jwtToken);
    const loggedInUserID = jwt.verify(jwtToken, SECRET_KEY, (err, payload) => {
        if (err) {
            console.log(err);
            return null;
        } else {
            console.log(payload);
            const name = payload.username;
            console.log("UserId from Payload: ", name);
            return name;
        }
    });

    return loggedInUserID;
};

module.exports = decodeJWT;
