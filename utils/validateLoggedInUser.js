const { ObjectId } = require("mongodb");
const LoginDetails = require("../models/login/login-details");
const decodeJWT = require("../utils/decodeJWT");

// * Function to validate user
const validateLoggedInUser = async (authorizationToken) => {
    try {
        // * Decode authorization token of logged-in user.
        let loggedInUser = decodeJWT(authorizationToken);

        console.log("Current LoggedIn User Id: ", loggedInUser);

        if (loggedInUser == null) {
            return {
                success: false,
                message: "Invalid authorization token!",
            };
        }

        // * Search for user_type of logged-in user from User table via decoded user ID.

        const userDetails = await LoginDetails.findOne({
            username: loggedInUser,
        });

        console.log("User Details", userDetails);
        // * validate if user searched through id does not exist return failure response
        if (!userDetails) {
            return {
                success: false,
                message: "User does not exist!",
            };
        } else {
            return {
                success: true,
                message: "User found!",
                data: loggedInUser,
            };
        }
    } catch (error) {
        console.log(new Date(), error);
        return {
            success: false,
            message: error,
        };
    }
};

module.exports = validateLoggedInUser;
