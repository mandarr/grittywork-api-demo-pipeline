// const Agenda = require('agenda');
const agenda = require("../db/agenda");

const createNewReminder = async (name, timeInterval, data) => {
    await agenda.start();
    console.log("Agenda here", timeInterval, name, data);
    await agenda.schedule(timeInterval, name, data);
};

const createReminderJob = async (name, callback) => {
    await agenda.start();
    agenda.define(name, async (job) => {
        callback(job.attrs);
    });
};

// * delete reminders by name.
const deleteReminderByName = async (jobName) => {
    console.log("Name:", jobName);
    await agenda.start();
    let removedJobResponse = await agenda.cancel({ "data._id": jobName });
    console.log("Response after removing jobs:", removedJobResponse);
};

module.exports = {
    createNewReminder,
    createReminderJob,
    deleteReminderByName,
};
