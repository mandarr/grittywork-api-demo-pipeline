const mongoose = require("mongoose");

const questions = new mongoose.Schema({
    question_title: {
        type: String,
        required: true,
    },
    question_type: {
        type: String,
        required: true,
    },
    survey_id: {
        type: String,
        required: true,
    },
    ans_code: {
        type: String,
        required: true,
    },
    created_at: {
        type: Number,
        required: true,
    },
    modified_at: {
        type: Number,
        required: true,
    },
    created_by: {
        type: String,
        required: true,
    },
    modified_by: {
        type: String,
        required: true,
    },
    is_mandatory: {
        type: Boolean,
        required: true,
    },
    is_active: {
        type: Boolean,
        required: true,
    },
    is_pulsed: {
        type: Boolean,
        required: true,
    },
    label: {
        type: String,
    },
    min_value: {
        type: Number,
    },
    max_value: {
        type: Number,
    },
    is_numeric: {
        type: Boolean,
    },
    parent_question_id: {
        type: String,
    },
    keyboard_type: {
        type: String,
    },
    placeholder: {
        type: String,
    },
    caption: {
        type: String,
    },
    order: {
        type: Number,
    },
    isSelected: {
        type: Boolean,
    },
    max_text: {
        type: String,
    },
    min_text: {
        type: String,
    },
});

module.exports = mongoose.model("question", questions);
