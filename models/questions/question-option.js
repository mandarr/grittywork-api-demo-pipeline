const mongoose = require("mongoose");

const question_option = new mongoose.Schema(
  {
    option_text: {
      type: String,
      required: true,
    },
    option_value: {
      type: String,
      required: true,
    },
    question_id: {
      type: Number,
      required: true,
    },
    survey_id: {
      type: Number,
      required: true,
    },
    is_active: {
      type: Boolean,
      required: true,
    },
  },
  { timestamps: { createdAt: "created_at", updatedAt: "modified_at" } },

  { collection: "questions_options" }
);

module.exports = mongoose.model("questions_options", question_option);
