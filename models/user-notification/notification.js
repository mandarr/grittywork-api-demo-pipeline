const mongoose = require("mongoose");

const Notification = new mongoose.Schema({
    title: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    schedule_type: {
        type: String,
        required: true,
    },
    schedule_date: {
        type: Date,
        required: true,
    },
    schedule_time: {
        type: Date,
        required: true,
    },
    notification_recipient: {
        type: [String],
        // required: true,
    },
    notification_recipient_type: {
        type: String,
        required: true,
    },
    is_active: {
        type: Boolean,
        required: true,
    },
    created_at: {
        type: Number,
        required: true,
    },
    created_by: {
        type: String,
    },
    modified_at: {
        type: Number,
        required: true,
    },
    modified_by: {
        type: String,
    },
});

module.exports = mongoose.model("Notification", Notification);
