const mongoose = require("mongoose");

const userNotification = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    user_id: {
      type: String,
      required: true,
    },
    notification_id: {
      type: String,
      required: true,
    },
    created_at: {
      type: Number,
      required: true,
    },
    created_by: {
      type: String,
    },
    modified_at: {
      type: Number,
      required: true,
    },
    modified_by: {
      type: String,
    },
    is_active: {
      type: Boolean,
      required: true,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("userNotification", userNotification);
