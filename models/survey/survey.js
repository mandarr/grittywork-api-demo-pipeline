const mongoose = require("mongoose");

const survey = new mongoose.Schema(
  {
    survey_name: {
      type: String,
      required: true,
    },
    is_active: {
      type: Boolean,
      required: true,
    },
    created_by: {
      type: String,
      required: true,
    },
    modified_by: {
      type: String,
      required: true,
    },
    created_at: {
      type: Number,
      required: true,
    },
    modified_at: {
      type: Number,
      required: true,
    },
    is_pulsed_active: {
      type: Boolean,
      required: true,
    },
  },
  { collection: "survey" }
);

module.exports = mongoose.model("survey", survey);
