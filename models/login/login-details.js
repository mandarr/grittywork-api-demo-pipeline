const mongoose = require("mongoose");

const loginDetailsSchema = new mongoose.Schema(
    {
        username: {
            type: String,
            required: true,
            unique: true,
        },
        fcm_token: {
            type: String,
            required: true,
            // unique: true,
        },
        user_type: {
            type: String,
            required: true,
        },
        data_use: {
            type: Boolean,
            required: true,
        },
        created_at: {
            type: Number,
            required: true,
        },
        modified_at: {
            type: Number,
            required: true,
        },
    },

    //   Renames the collection
    { collection: "login-details" }
);

module.exports = mongoose.model("login-details", loginDetailsSchema);
