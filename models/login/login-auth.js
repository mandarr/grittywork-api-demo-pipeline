const mongoose = require("mongoose");

const loginAuthSchema = new mongoose.Schema(
    {
        password: {
            type: String,
            required: true,
        },
        refresh_token: {
            type: String,
            required: true,
            // unique: true,
        },
        username: {
            type: String,
            required: true,
            unique: true,
        },
        user_type: {
            type: Object,
            required: true,
            admin: Boolean,
            local: Boolean,
        },
        created_at: {
            type: Number,
            required: true,
        },
        modified_at: {
            type: Number,
            required: true,
        },
    },

    //   Renames the collection
    { collection: "login-auth" }
);

module.exports = mongoose.model("login-auth", loginAuthSchema);
