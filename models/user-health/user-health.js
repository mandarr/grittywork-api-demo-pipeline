const mongoose = require("mongoose");

const userHealth = new mongoose.Schema(
    {
        noOfSteps: {
            type: Number,
            required: true,
        },
        hoursOfSleep: {
            type: String,
            required: true,
        },
        date: {
            type: String,
            required: true,
        },
        username: {
            type: String,
            required: true,
        },
    },
    { collection: "user-health-data" }
);

module.exports = mongoose.model("user-health", userHealth);
