const mongoose = require("mongoose");

const cachedSurveyData = new mongoose.Schema(
    {
        list: {
            type: Array,
            required: true,
        },
        survey_id: {
            type: String,
            required: true,
        },
        user_id: {
            type: String,
            required: true,
        },
        date: {
            type: Number,
            required: false,
        },
        currentQuiz: {
            type: Number,
            required: false,
        },
    },
    { collection: "cached-survey-data" }
);

module.exports = mongoose.model("cached-survey-data", cachedSurveyData);
