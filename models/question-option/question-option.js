const mongoose = require("mongoose");

const questionOption = new mongoose.Schema({
    option_text: {
        type: String,
        // required: false,
    },
    option_value: {
        type: String,
        // required: false,
    },
    image: {
        type: String,
    },
    question_id: {
        type: String,
        required: true,
    },
    created_at: {
        type: Number,
        required: true,
    },
    modified_at: {
        type: Number,
        required: true,
    },
    created_by: {
        type: String,
        required: true,
    },
    modified_by: {
        type: String,
        required: true,
    },
    is_active: {
        type: Boolean,
        required: true,
    },
    isSelected: {
        type: Boolean,
        required: true,
    },
    image: {
        type: String,
    },
    order: {
        type: Number,
    },
});

module.exports = mongoose.model("question-option", questionOption);
