const mongoose = require("mongoose");

const answer = new mongoose.Schema(
  {
    user_id: {
      type: String,
      required: true,
    },

    survey_id: {
      type: String,
      required: true,
    },
    is_active: {
      type: Boolean,
      // required: true,
    },
    is_complete: {
      type: Boolean,
    },
    created_at: {
      type: Number,
      required: true,
    },
    ans_code_1: {
      type: String,
    },

    ans_code_2: {
      type: String,
    },

    ans_code_3: {
      type: String,
    },

    ans_code_4: {
      type: String,
    },

    ans_code_5: {
      type: String,
    },

    ans_code_6: {
      type: String,
    },

    ans_code_7: {
      type: String,
    },

    ans_code_8: {
      type: String,
    },

    ans_code_9: {
      type: String,
    },

    ans_code_10: {
      type: String,
    },

    ans_code_11: {
      type: String,
    },

    ans_code_12: {
      type: String,
    },

    ans_code_13: {
      type: String,
    },

    ans_code_14: {
      type: String,
    },

    ans_code_15: {
      type: String,
    },

    ans_code_16: {
      type: String,
    },

    ans_code_17: {
      type: String,
    },

    ans_code_18: {
      type: String,
    },

    ans_code_19: {
      type: String,
    },

    ans_code_20: {
      type: String,
    },

    ans_code_21: {
      type: String,
    },

    ans_code_22: {
      type: String,
    },

    ans_code_23: {
      type: String,
    },

    ans_code_24: {
      type: String,
    },

    ans_code_25: {
      type: String,
    },

    ans_code_26: {
      type: String,
    },

    ans_code_27: {
      type: String,
    },

    ans_code_28: {
      type: String,
    },

    ans_code_29: {
      type: String,
    },

    ans_code_30: {
      type: String,
    },

    ans_code_31: {
      type: String,
    },

    ans_code_32: {
      type: String,
    },

    ans_code_33: {
      type: String,
    },

    ans_code_34: {
      type: String,
    },

    ans_code_35: {
      type: String,
    },

    ans_code_36: {
      type: String,
    },

    ans_code_37: {
      type: String,
    },

    ans_code_38: {
      type: String,
    },

    ans_code_39: {
      type: String,
    },

    ans_code_40: {
      type: String,
    },

    ans_code_41: {
      type: String,
    },

    ans_code_42: {
      type: String,
    },

    ans_code_43: {
      type: String,
    },

    ans_code_44: {
      type: String,
    },

    ans_code_45: {
      type: String,
    },

    ans_code_46: {
      type: String,
    },

    ans_code_47: {
      type: String,
    },

    ans_code_48: {
      type: String,
    },

    ans_code_49: {
      type: String,
    },

    ans_code_50: {
      type: String,
    },
  },
  { collection: "answer" }
);

module.exports = mongoose.model("answer", answer);
