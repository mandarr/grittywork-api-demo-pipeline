module.exports = {
    apps: [
        {
            name: "weworksmart-api",
            script: "./server.js",
        },
        {
            name: "weworksmart-reminder",
            script: "./scripts/reminder/reminder-cron.js",
        },
    ],
};
