const { Agenda } = require("agenda");
require("dotenv").config();

const connectionOpts = {
    db: {
        address: process.env.MONGODB_URI,
        collection: "agendaJobs",
        options: { useUnifiedTopology: true },
    },
};
console.log(connectionOpts);

const agenda = new Agenda(connectionOpts);

agenda.start(); // Returns a promise, which should be handled appropriately

module.exports = agenda;
