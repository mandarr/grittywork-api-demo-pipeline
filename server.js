const express = require("express");
const cors = require("cors");
const app = express();
const bodyParser = require("body-parser");

// Cors
app.use(cors());

// Parse json data
app.use(express.json());

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

// Routers
const loginRouter = require("./routes/login");
const questionRouter = require("./routes/question");
const questionOptionRouter = require("./routes/question-option");
const surveyRouter = require("./routes/survey");
const userHealthRouter = require("./routes/user-health");
const ansRouter = require("./routes/answer");
const userNotification = require("./routes/user-notification");
const csvRouter = require("./routes/download-csv");
const cacheDataRouter = require("./routes/cache-data");

// Read dotenv files
require("dotenv").config();

// User routes
app.use("/api/v1/user", loginRouter);

// Survey  & Questions routes
app.use("/api/v1/survey/question", questionRouter);

// Question option routes
app.use("/api/v1/survey/questionoption", questionOptionRouter);

// Survey routes
app.use("/api/v1/survey", surveyRouter);

// User Health routes
app.use("/api/v1/userHealth", userHealthRouter);

// Ans Router
app.use("/api/v1/ans", ansRouter);

// User Notification
app.use("/api/v1/notifications", userNotification);

// Download survey data
app.use("/api/v1/downloadcsv", csvRouter);

// Cache data router
app.use("/api/v1/cache", cacheDataRouter);

app.get("/api/v1", (req, res) => {
    res.json({ status: "success", message: "API Version 1.0" });
});

app.get("/api/v1/info", (req, res) => {
    console.log("GET /api/v1/info");
    res.json({ status: "success", version: "1.0", name: "GrittyWork API" });
});

// mongoose
const mongoose = require("mongoose");

// mongodb connect
mongoose.connect(
    process.env.MONGODB_URI,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    },
    (err) => {
        if (err) throw err;
        console.log("Connected to MongoDB..");
    }
);

// PORT
const PORT = 8000;

// Listen to PORT
app.listen(PORT, () => {
    console.log(`Server is running on http://localhost:${PORT}/api/v1`);
});
