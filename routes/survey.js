const express = require("express");
const router = express.Router();
const passport = require("passport");

// Survey controller
const surveyController = require("../controllers/survey/surveyController");

// Auth middleware
const auth = require("../middlewares/auth");

// Initialize passport for authentication
router.use(passport.initialize());

router.get("/cms", auth, surveyController.getCmsSurveyList);

// list of surveys
router.get("/all", auth, surveyController.getSurveyList);

// survey by id
router.get("/", auth, surveyController.getSurvey);

// create survey
router.post("/create", auth, surveyController.createSurvey);

// update survey
router.put("/update", auth, surveyController.updateSurvey);

module.exports = router;
