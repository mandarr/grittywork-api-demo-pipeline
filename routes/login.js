const express = require("express");
const router = express.Router();
const passport = require("passport");

// Login controller
const loginControllers = require("../controllers/login/loginController");

// Auth middleware
const auth = require("../middlewares/auth");

// Initialize passport for authentication
router.use(passport.initialize());

// Register user
router.post("/register", loginControllers.userRegister);

// Login User
router.post("/login", loginControllers.userLogin);

// Get all users
router.get("/all", auth, loginControllers.getAllUsers);

// Get user by id
router.get("/", auth, loginControllers.getUser);

// Update user info
router.put("/update", auth, loginControllers.updateUserInfo);

// Generate new access-token from refresh-token
router.post("/refresh", loginControllers.getAccessToken);

// Get data-use status for user
router.get("/data-use-status", loginControllers.getDataUseStatus);

// Update data-use status
router.put("/update-data-use-status", loginControllers.updateDataUseStatus);

module.exports = router;
