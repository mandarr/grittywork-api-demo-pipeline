const express = require("express");
const router = express.Router();
const passport = require("passport");

// Notification controller
const userNotificationController = require("../controllers/user-notification/user-notification");

// Auth middleware
const auth = require("../middlewares/auth");

// Initialize passport for authentication
router.use(passport.initialize());

// Get user notification using id
router.get("/", auth, userNotificationController.getUserNotificationDetails);

// Get all notification list
router.get("/all", auth, userNotificationController.getAllNotification);

// Get type notification list
router.get("/type", auth, userNotificationController.getTypeNotification);

// Get notification using id
router.get(
    "/:notificationId",
    userNotificationController.getNotificationDetails
);

// Create notification
router.post("/create", auth, userNotificationController.createNotification);

// Update notification
router.put(
    "/update/:notificationId",
    userNotificationController.updateNotification
);

// Delete notification
router.delete("/delete", auth, userNotificationController.deleteNotification);

module.exports = router;
