const express = require("express");
const router = express.Router();
const passport = require("passport");

// Auth middleware
const auth = require("../middlewares/auth");

// Initialize passport for authentication
router.use(passport.initialize());

const userHealthController = require("../controllers/user-health/userHealthController");

//  get health data
router.get("/", auth, userHealthController.getHealthData);

// post health data
router.post("/create", auth, userHealthController.postHealthData);

// update health data
router.put("/update", auth, userHealthController.updateHealthData);

module.exports = router;
