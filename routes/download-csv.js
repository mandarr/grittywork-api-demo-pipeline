const express = require("express");
const router = express.Router();
const passport = require("passport");

const csvController = require("../controllers/download-csv/download-ans-csv");

// Auth middleware
const auth = require("../middlewares/auth");

// Initialize passport for authentication
router.use(passport.initialize());

router.get("/survey", auth, csvController.downloadSurveyCsv);
router.get("/health", auth, csvController.downloadHealthCsv);

module.exports = router;
