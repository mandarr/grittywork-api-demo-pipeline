const express = require("express");
const router = express.Router();
const passport = require("passport");

// Question controller
const questionController = require("../controllers/question/questionController");

// Auth middleware
const auth = require("../middlewares/auth");

// Initialize passport for authentication
router.use(passport.initialize());

// Get all questions
router.get("/", auth, questionController.getAllQuestions);

// Get question by id
// router.get("/", questionController.getQuestion);

// Create new question
router.post("/create", auth, questionController.createQuestion);

// Update question by id--
router.put("/update", auth, questionController.updateQuestion);

module.exports = router;
