const express = require("express");
const router = express.Router();
const passport = require("passport");

// Cache Data controller
const cachedDataController = require("../controllers/cache-survey-data/cacheDataController");

// Auth middleware
const auth = require("../middlewares/auth");

// Initialize passport for authentication
router.use(passport.initialize());

// Get cached data from db
router.get("/", cachedDataController.getData);

// Post surveyList to db
router.post("/create", auth, cachedDataController.postData);

// Update surveyList in db
router.put("/update", auth, cachedDataController.updateData);

// Delete surveyList after completion of survey
router.delete("/remove", auth, cachedDataController.removeData);

module.exports = router;
