const express = require("express");
const router = express.Router();
const passport = require("passport");

// Answer controller
const ansController = require("../controllers/answer/answer");

// Auth Middleware
const auth = require("../middlewares/auth");

// Initialize passport for authentication
router.use(passport.initialize());

// Get Answers
router.get("/", auth, ansController.getAnswer);

// Get pending surveys
router.get("/pendingSurvey", auth, ansController.getPendingSurvey);

// Create Answers
router.post("/create", auth, ansController.postAnswer);

// Update Answers
router.put("/update", auth, ansController.updateAnswer);
router.get("/latestSurvey", auth, ansController.getLatestSurvey);

module.exports = router;
