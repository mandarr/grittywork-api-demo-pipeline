const express = require("express");
const router = express.Router();
const passport = require("passport");

const questionOption = require("../controllers/question-option/question-option");

// Auth middleware
const auth = require("../middlewares/auth");

// Initialize passport for authentication
router.use(passport.initialize());

router.get("/", auth, questionOption.getOptionData);

router.post("/create", auth, questionOption.createOptionData);

module.exports = router;
