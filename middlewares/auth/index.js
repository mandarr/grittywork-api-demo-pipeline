const passport = require("passport");

require("../auth/passport");

const auth = (req, res, next) => {
    passport.authenticate("jwt", { session: false }, (err, user, info) => {
        if (err || !user) {
            // Custom error response
            const err = {};
            err.status = "error";
            err.message = "Unauthorized";

            return res.status(401).json(err); // send the error response to client
        }
        return next(); // continue to next middleware if no error.
    })(req, res, next); /* passport.authentication returns a function,
                         we invoke it with normal req..res arguments 
                         to override default functionality */
};

module.exports = auth;
