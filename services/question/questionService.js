require("dotenv").config;

const Question = require("../../models/questions/question");
const QuestionOption = require("../../models/question-option/question-option");

const getQuestionBySurvey = async (surveyId) => {
  const questions = await Question.find({ survey_id: surveyId }).lean();
  const options = await QuestionOption.find({ survey_id: surveyId }).lean();

  let _questions = questions.map((item) => {
    let filteredOpt = [];

    if (item.question_type !== "slider_group") {
      filteredOpt = options
        .filter(
          (element) => element.question_id.toString() === item._id.toString()
        )
        .sort(function (a, b) {
          return a.order - b.order;
        });
      return { ...item, options: filteredOpt };
    } else {
      filteredOpt = questions
        .filter(
          (element) =>
            element.parent_question_id.toString() === item._id.toString()
        )
        .sort(function (a, b) {
          return a.order - b.order;
        });
      return { ...item, options: filteredOpt };
    }
  });
  const filteredQuestions = _questions.filter((item) => {
    return item.question_type !== "slider";
  });
  // const points = [40, 100, 1, 5, 25, 10];
  filteredQuestions.sort(function (a, b) {
    return a.order - b.order;
  });
  return filteredQuestions;
};

module.exports = {
  getQuestionBySurvey,
};
