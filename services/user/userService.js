require("dotenv").config;

const LoginDetails = require("../../models/login/login-details");

const getUserById = async (userId) => {
    const user = await LoginDetails.findOne({ _id: userId }).lean();
    return user;
};

module.exports = {
    getUserById,
};
