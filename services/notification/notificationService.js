require("dotenv").config;

const loginDetails = require("../../models/login/login-details");
const userNotification = require("../../models/user-notification/user-notification");
const Notification = require("../../models/user-notification/notification");
var FCM = require("fcm-node");
var serverKey = process.env.SERVER_KEY;

let fcm = new FCM(serverKey);

const sendNotificationFCM = async (notification, fcmToken, userId) => {
    const parseHtml = (data) => {
        var htmlString = data;
        var plainString = htmlString.replace(/<[^>]+>/g, "");
        return plainString;
    };
    try {
        var message = {
            to: fcmToken,
            content_available: true,
            priority: "high",
            notification: {
                title: notification.title,
                body: parseHtml(notification.description),
                icon: "ic_notification",
                sound: "default",
            },
            data: {
                id: userId,
                notificationId: notification._id,
            },
        };
    } catch (error) {
        console.log(error);
    }

    fcm.send(message, function (err, response) {
        if (err) {
            console.log("Something has gone wrong!", err);
        } else {
            console.log("Successfully sent with response: ", response);
        }
    });
};

const createUserNotification = async (userIds, notification) => {
    console.log("Inside createNotification", notification);
    userIds.forEach(async (element) => {
        const response = await loginDetails.findOne({
            _id: element,
        });

        // console.log(element);
        await userNotification.create({
            title: notification.title,
            description: notification.description,
            notification_id: notification._id,
            user_id: element,
            created_at: notification.schedule_date,
            created_by: notification.created_by,
            modified_at: notification.modified_at,
            modified_by: notification.modified_by,
            is_active: notification.is_active,
        });

        sendNotificationFCM(notification, response.fcm_token, element);
        if (notification.schedule_type !== "now") {
            await Notification.findByIdAndUpdate(notification._id, {
                schedule_type: "now",
                schedule_time: notification.schedule_time,
            });
        }
    });
};

module.exports = {
    sendNotificationFCM,
    createUserNotification,
};
