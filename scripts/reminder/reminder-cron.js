require("dotenv").config();
const mongoose = require("mongoose");
const loginDetails = require("../../models/login/login-details");

// Notification service
const {
    createUserNotification,
} = require("../../services/notification/notificationService");

// Agenda Helper functions
const {
    createReminderJob,
    deleteReminderByName,
} = require("../../utils/agendaHelper");

// mongodb connect
mongoose.connect(
    process.env.MONGODB_URI,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    },
    (err) => {
        if (err) throw err;
        console.log("Connected to MongoDB..");
    }
);

const functionToCreateReminderJob = async (data) => {
    const allUsers = await loginDetails.find({});
    let allUsersId = allUsers.map((item) => {
        return item._id.toString();
    });
    console.log(allUsersId);
    console.log("data.notification_recipient", data.notification_recipient)
    if (data.notification_recipient_type === "all") {
        createUserNotification(allUsersId, data);
    } else {
        console.log("Selected Users");
        createUserNotification(data.notification_recipient, data);
    }
};

async function run() {
    createReminderJob("Push Notification", async (jobAttrs) => {
        functionToCreateReminderJob(jobAttrs.data);
        deleteReminderByName(jobAttrs.data._id);
    });
}
run().catch((error) => {
    console.error(error);
    process.exit(-1);
});
