const express = require("express");
const answer = require("../../models/answers/answer");
require("dotenv").config;

const Answer = require("../../models/answers/answer");
const CachedSurveyData = require("../../models/cached-survey-data/cached-survey-data");
const LoginDetails = require("../../models/login/login-details");
const Question = require("../../models/questions/question");
const Survey = require("../../models/survey/survey");

// utils
const validateLoggedInUser = require("../../utils/validateLoggedInUser");

const getAnswer = async (req, res) => {
    try {
        const { surveyId, date } = req.query;
        let loggedInUser = await validateLoggedInUser(
            req.headers.authorization
        );
        console.log("loggedInUser:", loggedInUser);
        if (surveyId && date) {
            const user = await LoginDetails.findOne({
                username: loggedInUser.data,
            });

            console.log("user:", user);
            if (!user) {
                return res
                    .status(404)
                    .json({ status: "error", message: "User not found." });
            }

            const data = await Answer.findOne({
                survey_id: surveyId,
                user_id: user._id,
                date,
            });

            console.log(data);

            res.status(200).json({
                status: "success",
                answer_id: data != null ? data._id : null,
            });
        } else {
            return res.status(400).json({
                status: "error",
                message: "Incomplete data provided.",
            });
        }
    } catch (error) {
        return res.status(400).json({
            status: "error",
            message: "Failed.",
            error: error,
        });
    }
};

const postAnswer = async (req, res) => {
    try {
        const { surveyId, ans_code, ans } = req.body;
        let loggedInUser = await validateLoggedInUser(
            req.headers.authorization
        );
        console.log(req.body);
        if (surveyId && ans_code && ans) {
            const user = await LoginDetails.findOne({
                username: loggedInUser.data,
            });
            if (!user) {
                return res
                    .status(404)
                    .json({ status: "error", message: "User not found." });
            }

            const data = await Answer.create({
                user_id: user._id,
                survey_id: surveyId,
                created_at: Date.now(),
                [ans_code]: ans,
                is_complete: false,
            });
            return res
                .status(200)
                .json({ status: "success", answer_id: data._id });
        } else {
            return res.status(400).json({
                status: "error",
                message: "Incomplete data provided.",
            });
        }
    } catch (err) {
        return res.status(400).json({
            status: "error",
            message: "Failed to post answer.",
        });
    }
};

const updateAnswer = async (req, res) => {
    try {
        const { ansId, is_complete } = req.query;
        const ansObject = req.body;

        let queryBody = {
            created_at: Date.now(),
            is_complete,
        };

        for (let index = 0; index < ansObject.length; index++) {
            let ansData = ansObject[index];
            queryBody[ansData.ansCode] = ansData.ans;
        }

        console.log(queryBody);
        if (ansId && is_complete) {
            const data = await Answer.findByIdAndUpdate(ansId, queryBody);
            console.log(data);
            return res
                .status(200)
                .json({ status: "success", message: "Answer updated." });
        } else {
            return res.status(400).json({
                status: "error",
                message: "Incomplete data provided.",
            });
        }
    } catch (err) {
        return res.status(400).json({
            status: "error",
            message: "Failed to post answer.",
        });
    }
};

const getPendingSurvey = async (req, res) => {
    try {
        const username = req.query.username;
        let loggedInUser = await validateLoggedInUser(
            req.headers.authorization
        );

        const user = await LoginDetails.findOne({
            username: loggedInUser.data,
        });
        // if (!user) {
        //     return res
        //         .status(404)
        //         .json({ status: "error", message: "User not found." });
        // }

        const answer = await Answer.find({
            user_id: user._id,
            is_complete: false,
        });

        console.log("Answer object ", answer);

        const cacheData = await CachedSurveyData.findOne({
            user_id: user._id,
            survey_id: answer[0]?.survey_id,
        });
        console.log(cacheData);
        return res.status(200).json({
            status: "success",
            list: answer,
            currentQuiz: cacheData == null ? null : cacheData?.currentQuiz,
        });
    } catch (error) {
        console.log(error);
    }
};

const getLatestSurvey = async (req, res) => {
    try {
        const username = req.query.username;
        let loggedInUser = await validateLoggedInUser(
            req.headers.authorization
        );
        const user = await LoginDetails.findOne({
            username: loggedInUser.data,
        });
        const data = await Answer.findOne({
            user_id: user._id,
            is_complete: true,
        })
            .limit(1)
            .sort({ $natural: -1 });
        return res.status(200).json({
            status: "success",
            date: data != null ? data.created_at : null,
        });
    } catch (error) {
        console.log(error);
    }
};

module.exports = {
    getAnswer,
    postAnswer,
    updateAnswer,
    getPendingSurvey,
    getLatestSurvey,
};
