const express = require("express");
require("dotenv").config;
const moment = require("moment-timezone");

// models
const LoginDetails = require("../../models/login/login-details");
const Notification = require("../../models/user-notification/notification");
const UserNotification = require("../../models/user-notification/user-notification");
const {
    createUserNotification,
} = require("../../services/notification/notificationService");

// Services
const {
    createNewReminder,
    deleteReminderByName,
} = require("../../utils/agendaHelper");
const validateLoggedInUser = require("../../utils/validateLoggedInUser");

const getAllNotification = async (req, res) => {
    try {
        const { username } = req.query;
        let loggedInUser = await validateLoggedInUser(
            req.headers.authorization
        );

        const user = await LoginDetails.findOne({
            username: loggedInUser.data,
        });

        console.log(user);
        const _notification = await UserNotification.find({
            user_id: user._id,
        });
        return res.status(200).json({ status: "success", list: _notification });
    } catch (error) {
        return res.status(400).json({
            status: "error",
            message: "Failed to fetch notification.",
        });
    }
};

// Get notification by schedule type
const getTypeNotification = async (req, res) => {
    try {
        const type = req.query.schedule_type;

        if (type === "now" || type === "later") {
            const notification = await Notification.find({
                schedule_type: type,
            });
            return res.status(200).json({
                status: "Success",
                notification: notification,
            });
        } else {
            return res.status(400).json({
                status: "error",
            });
        }
    } catch (error) {
        return res.status(400).json({
            status: "Error",
            message: "Failed to fetch notification.",
        });
    }
};

const getNotificationSelectedUser = async (req, res) => {
    try {
        const notificationId = req.query;

        const notification = await Notification.findOne({
            _id: notificationId,
        }).lean();

        const data = notification.notification_receipants;
        const ids = data.split(",");
        console.log(ids);
        const allUsers = await LoginDetails.find({}).lean();
        let allUsersId = allUsers.map((item, index) => {
            if (item._id.toString() === ids[index]) {
                return {
                    ...item,
                    label: item.username,
                    value: item._id,
                    isSelected: true,
                };
            } else {
                return {
                    ...item,
                    label: item.username,
                    value: item._id,
                    isSelected: false,
                };
            }
        });
        return res.status(200).json({ status: "success", list: allUsersId });
    } catch (error) {
        return res.status(400).json({
            status: "error",
            message: error.message,
        });
    }
};

// Get notification details by Id
const getNotificationDetails = async (req, res) => {
    try {
        console.log(req.params);
        const { notificationId } = req.params;

        const notification = await Notification.findOne({
            _id: notificationId,
        });
        return res
            .status(200)
            .json({ status: "success", notification: notification });
    } catch (error) {
        return res.status(400).json({
            status: "Error",
            message: "Failed to fetch notification.",
        });
    }
};

// Get notification details by notificationId & userId
const getUserNotificationDetails = async (req, res) => {
    try {
        const notificationId = req.query.notificationId;
        const userId = req.query.userId;

        if (notificationId && userId) {
            const notification = await UserNotification.findOne({
                notification_id: notificationId,
                user_id: userId,
            });

            res.status(200).json({
                status: "success",
                notification: notification,
            });
        } else {
            res.status(400).json({
                status: "error",
                message: "Please provide notificationId/userId",
            });
        }
    } catch (error) {
        return res.status(400).json({
            status: "Error",
            message: "Failed to fetch notification.",
        });
    }
};

// Create new notificaton
const createNotification = async (req, res) => {
    try {
        const {
            title,
            description,
            schedule_type,
            schedule_date,
            is_active,
            notification_recipient,
            schedule_time,
            notification_recipient_type,
        } = req.body;
        let loggedInUser = await validateLoggedInUser(
            req.headers.authorization
        );

        // For user who created notification i.e. admin
        const user = await LoginDetails.find({
            username: loggedInUser.data,
        }).lean();

        // All users from logindetails db
        const allUsers = await LoginDetails.find({});
        let allUsersId = allUsers.map((item) => {
            return item._id.toString();
        });

        if (!user) {
            return res.status(404).json({
                status: "Error",
                message: "User not found",
            });
        }

        const notification = await Notification.create({
            username: loggedInUser.data,
            title,
            description,
            schedule_type,
            schedule_date: schedule_date,
            notification_recipient:
                notification_recipient_type === "all"
                    ? ""
                    : notification_recipient,

            schedule_time: schedule_time,
            notification_recipient_type,
            is_active,
            created_at: Date.now(),
            created_by: user._id,
            modified_at: 0,
            modified_by: "null",
        });

        const ids = allUsersId;
        if (schedule_type === "now") {
            if (notification_recipient_type === "all") {
                createUserNotification(ids, notification);
            } else {
                createUserNotification(notification_recipient, notification);
            }
        } else {
            console.log("notification data for rminder", notification);
            // ! Currently working on agenda-cron
            createNewReminder(
                "Push Notification",
                // moment.tz(notification.schedule_time, "US/Mountain").format(),
                notification.schedule_time,
                notification
            );
        }

        return res.status(200).json({
            status: "success",
            message: "Notification created successfully",
            notification: notification,
        });
    } catch (error) {
        console.log(error);
        return res.status(400).json({
            status: "error",
            message: error.message,
        });
    }
};

// Update notification
const updateNotification = async (req, res) => {
    try {
        const notificationId = req.params.notificationId;
        let loggedInUser = await validateLoggedInUser(
            req.headers.authorization
        );

        const {
            username,
            title,
            description,
            schedule_type,
            schedule_date,
            schedule_time,
            notification_recipient,
            notification_recipient_type,
            is_active,
        } = req.body;
        const user = await LoginDetails.findOne({
            username: loggedInUser.data,
        });

        // All users from logindetails db
        const allUsers = await LoginDetails.find({});
        let allUsersId = allUsers.map((item) => {
            return item._id.toString();
        });

        if (!user) {
            return res.status(200).json({
                status: "error",
                message: "User not found",
            });
        }
        const data = {
            username: loggedInUser.data,
            title,
            description,
            schedule_type,
            modified_at: Date.now(),
            modified_by: user._id,
            notification_recipient:
                notification_recipient_type === "all"
                    ? ""
                    : notification_recipient,
            notification_recipient_type,
            schedule_date,
            schedule_time,
            is_active,
        };

        const notification = await Notification.findByIdAndUpdate(
            notificationId,
            data,
            { new: true }
        );

        const ids = allUsersId;
        if (schedule_type === "now") {
            await deleteReminderByName(notification._id);
            if (notification_recipient_type === "all") {
                createUserNotification(ids, notification);
            } else {
                createUserNotification(notification_recipient, notification);
            }
        } else {
            // ! Currently working on agenda-cron
            await deleteReminderByName(notification._id);
            createNewReminder(
                "Push Notification",
                // moment.tz(notification.schedule_time, "US/Mountain").format(),
                notification.schedule_time,
                notification
            );
        }
        res.status(200).json({
            status: "success",
            message: "Notification updated...",
        });
    } catch (error) {
        console.log("error", error);
        res.status(400).json({
            status: "error",
            message: "Failed to update.",
        });
    }
};

const deleteNotification = async (req, res) => {
    try {
        const notificationId = req.query.id;

        const notification = await Notification.findOneAndDelete({
            _id: notificationId,
        });
        console.log(
            "deleteNotification notification",
            notification,
            notificationId
        );
        await deleteReminderByName(notification._id);
        return res.status(200).json({
            status: "success",
            message: "Successfully deleted.",
        });
    } catch (error) {
        return res.status(400).json({
            status: "error",
            message: "Failed  to delete notification",
        });
    }
};

module.exports = {
    getAllNotification,
    getTypeNotification,
    getNotificationSelectedUser,
    getNotificationDetails,
    createNotification,
    updateNotification,
    deleteNotification,
    getUserNotificationDetails,
};
