const express = require("express");
require("dotenv").config;

const mongodb = require("mongodb").MongoClient;
const Json2csvParser = require("json2csv").Parser;
const fs = require("fs");
const moment = require("moment");
const download = require("download");

const Survey = require("../../models/survey/survey");
const Answer = require("../../models/answers/answer");
const UserHealthData = require("../../models/user-health-data/user-health-data");
const LoginDetails = require("../../models/login/login-details");

//Service
const {
  getQuestionBySurvey,
} = require("../../services/question/questionService");
const { getUserById } = require("../../services/user/userService");

const downloadSurveyCsv = async (req, res) => {
  const surveyId = req.query.surveyId;
  // const username = req.query.username;

  const _survey = await Survey.findOne({ surveyId });
  if (!_survey) {
    return res
      .status(404)
      .json({ status: "error", message: "Survey not found." });
  }

  // const user = await LoginDetails.findOne({ username });
  // if (!user) {
  //   return res.status(404).json({ status: "error", message: "User not found" });
  // }

  //get questions from survey
  let questions = await getQuestionBySurvey(surveyId);

  // console.log(_survey);
  // console.log(_survey.survey_name, _survey.created_at);
  // console.log("questions", surveyId, JSON.stringify(questions));

  let allQuestionSet = [...questions];
  questions.map((q) => {
    if (q.question_type == "slider_group") {
      console.log("slider group");
      allQuestionSet = [...allQuestionSet, ...q.options];
    }
  });
  console.log("-------------------------------------------");
  console.log("after0", JSON.stringify(allQuestionSet), "after1");

  allQuestionSet.sort(function (a, b) {
    return a.order - b.order;
  });

  let answers = await Answer.find({
    surveyId: surveyId,
  }).lean();

  if (answers.length > 0) {
    //update object names
    // survey = survey.map((item) => {
    //   return item.survey_name;
    // });
    // console.log(survey, "survey");
    // user = user.map((item) => {
    //   return item.username;
    // });
    let updatesAnswers = [];
    for (let data of answers) {
      console.log("data", data);
      let userData = await getUserById(data.user_id);

      console.log("data", userData);

      delete data.__v;
      delete data._id;

      data["Survey Name"] = _survey.survey_name;
      data["User Name"] = userData.username;
      delete data["user_id"];
      delete data["survey_id"];
      delete data["_id"];
      delete data["is_complete"];
      delete data["created_at"];
      allQuestionSet.map((q) => {
        // console.log(q, "ques");
        let oldKey = q.ans_code;
        let newKey = q.question_title;
        delete Object.assign(data, { [newKey]: data[oldKey] })[oldKey];
      });
      updatesAnswers.push(data);
    }
    console.log("answers", updatesAnswers);
    const json2csvParser = new Json2csvParser({ header: true });
    const csvData = json2csvParser.parse(updatesAnswers);

    const _date = moment(new Date()).format("MM-DD-YYYY");

    let fileName = `${_survey.survey_name}-${_date}.csv`;
    fs.writeFile(fileName, csvData, function (error) {
      res.setHeader("Content-Disposition", "attachment; filename=" + fileName);
      res.setHeader("Access-Control-Expose-Headers", "Content-Disposition");

      res.download(fileName);

      if (error) {
        // throw error;
        res.json({
          status: error.message,
          message: "Download failed",
        });
      }
    });
  } else {
    return res
      .status(404)
      .json({ status: "Not Found", message: "No Answers Find" });
  }
};

const downloadHealthCsv = async (req, res) => {
  let userHealthData = await UserHealthData.find().lean();
  console.log("user health data", userHealthData, userHealthData.length);

  if (userHealthData.length > 0) {
    userHealthData = userHealthData.map((data) => {
      delete data.__v;
      delete data._id;

      delete Object.assign(data, { ["User Name"]: data["username"] })[
        "username"
      ];
      delete Object.assign(data, { ["Date"]: data["date"] })["date"];
      delete Object.assign(data, { ["Steps"]: data["noOfSteps"] })["noOfSteps"];
      delete Object.assign(data, { ["Sleep"]: data["hoursOfSleep"] })[
        "hoursOfSleep"
      ];
      return data;
    });
    const json2csvParser = new Json2csvParser({ header: true });
    const csvData = json2csvParser.parse(userHealthData);
    const _date = moment(new Date()).format("MM-DD-YYYY");
    // console.log("run");

    // const { date } = req.body;
    // const _date = moment(date).format("DD-MM-YYYY");

    let fileName = `Health Data-${_date}.csv`;
    fs.writeFile(fileName, csvData, function (error) {
      if (error) {
        throw error;
      }

      res.setHeader("Content-Disposition", "attachment; filename=" + fileName);
      res.setHeader("Access-Control-Expose-Headers", "Content-Disposition");

      res.download(fileName);
      // console.log("File downloaded");
    });
  } else {
    return res
      .status(404)
      .json({ status: "Not Found", message: "Somthing went wrong" });
  }
};

module.exports = {
  downloadSurveyCsv,
  downloadHealthCsv,
};
