const moment = require("moment/moment");
const express = require("express");
require("dotenv").config;

// User health model
const UserHealth = require("../../models/user-health/user-health");

// Login details model
const LoginDetails = require("../../models/login/login-details");

// Services
const validateLoggedInUser = require("../../utils/validateLoggedInUser");

const getHealthData = async (req, res) => {
  try {
    const username = req.query.username;
    let loggedInUser = await validateLoggedInUser(req.headers.authorization);

    const user = await LoginDetails.findOne({ username: loggedInUser.data });

    if (!user) {
      return res.status(404).json({
        status: "Error",
        message: "User not found",
      });
    }

    // console.log(user);
    const _date = moment(Date.now()).format("MM-DD-YYYY");

    const data = await UserHealth.findOne({
      username: loggedInUser.data,
      date: _date,
    });

    console.log(data);

    res.status(200).json({
      status: "success",
      id: data != null ? data._id : null,
    });
  } catch (error) {
    return res.status(400).json({
      status: "Error",
      message: "Failed to get health data.",
    });
  }
};

const postHealthData = async (req, res) => {
  try {
    console.log(req.body);
    const { noOfSteps, hoursOfSleep } = req.body;
    let loggedInUser = await validateLoggedInUser(req.headers.authorization);
    console.log("loggedInUser =>", loggedInUser.data);

    if (noOfSteps && hoursOfSleep) {
      const user = await LoginDetails.findOne({ username: loggedInUser.data });
      console.log("user =>", user);
      if (!user) {
        return res.status(404).json({
          status: "Error",
          message: "User not found",
        });
      }
      const date = moment(Date.now()).format("MM-DD-YYYY");

      let _data = {
        noOfSteps,
        hoursOfSleep,
        date,
        username: loggedInUser.data,
      };
      const result = await UserHealth.create(_data);

      res.status(200).json({
        status: "success",
        id: result._id,
      });
    } else {
      return res.status(400).json({
        status: "error",
        message: "Invalid data provided.",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: "Error",
      message: "Failed to post health data.",
      // message: error.message,
    });
  }
};

const updateHealthData = async (req, res) => {
  try {
    const { dataId, noOfSteps, hoursOfSleep } = req.body;
    const data = await UserHealth.findByIdAndUpdate(dataId, {
      noOfSteps,
      hoursOfSleep,
    });

    res.status(200).json({
      status: "success",
      message: "User data updated.",
    });
  } catch (error) {
    return res.status(400).json({
      status: "Error",
      message: "Failed to update health data.",
    });
  }
};

module.exports = {
  postHealthData,
  updateHealthData,
  getHealthData,
};
