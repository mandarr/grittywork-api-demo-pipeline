const express = require("express");

const jwt = require("jsonwebtoken");

const bcrypt = require("bcryptjs");

require("dotenv").config();

const JWT_ACCESS_SECRET = process.env.SECRET;

const JWT_REFRESH_SECRET = process.env.RTSECRET;

const LoginAuth = require("../../models/login/login-auth");

const LoginDetails = require("../../models/login/login-details");

//Services
const { getUserById } = require("../../services/user/userService");
const validateLoggedInUser = require("../../utils/validateLoggedInUser");

// User register
const userRegister = async (req, res) => {
    try {
        const { username, password, user_type } = req.body;
        const hashedPassword = await bcrypt.hash(password, 10);

        const fcm_token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IB";

        await LoginDetails.create({
            username,
            user_type,
            fcm_token,
            data_use: false,
            created_at: Date.now(),
            modified_at: 0,
        });

        const refresh_token = "eyJhbGciOiJIUzI1NiIsInR5mRhciB";

        const user = await LoginAuth.create({
            username,
            password: hashedPassword,
            user_type,
            refresh_token,
            created_at: Date.now(),
            modified_at: 0,
        });

        console.log(user);
        res.status(200).json({
            status: "success",
            message: "Registration successful.",
        });
    } catch (err) {
        console.log("error", err);
        res.status(400).json({
            status: "error",
            message: "Error occured while registering user.",
        });
    }
};

// User login
const userLogin = async (req, res) => {
    const { username, password, fcm_token } = req.body;

    const user = await LoginAuth.findOne({ username }).lean();
    if (!user) {
        return res
            .status(401)
            .json({ status: "error", message: "Invalid Username or Password" });
    }
    if (username && password) {
        if (await bcrypt.compare(password, user.password)) {
            const accessToken = await jwt.sign(
                {
                    username: user.username,
                },
                JWT_ACCESS_SECRET,
                {
                    expiresIn: "1y",
                }
            );
            console.log("Access-Token: ", accessToken);

            const refreshToken = await jwt.sign(
                {
                    username: user.username,
                },
                JWT_REFRESH_SECRET,
                {
                    expiresIn: "1y",
                }
            );
            await LoginDetails.findOneAndUpdate(
                { username: username },
                { fcm_token: fcm_token }
            );
            await LoginAuth.findOneAndUpdate(
                { username: username },
                { refresh_token: refreshToken }
            );

            return res.status(200).json({
                status: "success",
                message: "Login successful.",
                username,
                access_token: accessToken,
                refresh_token: refreshToken,
                user_type: user.user_type,
            });
        } else {
            return res.status(401).json({
                status: "error",
                message: "Invalid Username or Password",
            });
        }
    } else {
        return res.status(401).json({
            status: "error",
            message: "Username/Password cannot be empty.",
        });
    }
};

// Get user by id
const getUser = async (req, res) => {
    const user = await getUserById(req.query.userId);
    res.status(200).json({ status: "success", user: user });
};

// Get all users
const getAllUsers = async (req, res) => {
    let loggedInUser = await validateLoggedInUser(req.headers.authorization);

    console.log("LoggedInUser:", loggedInUser);
    if (loggedInUser.success === true) {
        const users = await LoginDetails.find();
        res.status(200).json({ status: "success", users: users });
    } else {
        return {
            success: false,
            message: loggedInUser.message,
        };
    }
};

// Update user info
const updateUserInfo = async (req, res) => {
    let loggedInUser = await validateLoggedInUser(req.headers.authorization);

    const { username, password, fcm_token } = req.body;

    const user = await LoginAuth.findOne({
        username: loggedInUser.data,
    }).lean();

    if (user) {
        const hashedPassword = await bcrypt.hash(password, 10);

        if (password) {
            await LoginAuth.findOneAndUpdate(
                {
                    username,
                },
                {
                    password: hashedPassword,
                    modified_at: Date.now(),
                }
            );
        }

        if (fcm_token) {
            await LoginDetails.findOneAndUpdate(
                {
                    username,
                },
                {
                    fcm_token,
                    modified_at: Date.now(),
                }
            );
        }

        return res.status(200).json({
            status: "success",
            message: "User details updated.",
        });
    } else {
        return res.status(404).json({
            status: "error",
            message: "User not found.",
        });
    }
};

// Get new access-token
const getAccessToken = async (req, res) => {
    const { refresh_token } = req.body;
    const user = await LoginAuth.findOne({ refresh_token }).lean();

    if (!refresh_token) {
        return res.status(403).json({ message: "Forbidden" });
    }

    if (user.refresh_token === "eyJhbGciOiJIUzI1NiIsInR5mRhciB") {
        return res
            .status(403)
            .json({ message: "Login to generate refreshToken" });
    }
    const userData = { id: user._id, username: user.username };

    jwt.verify(refresh_token, JWT_REFRESH_SECRET, async (err, user) => {
        if (err) {
            return res.status(403);
        }

        // generate new access-token
        const accessToken = jwt.sign(userData, JWT_ACCESS_SECRET, {
            expiresIn: "1y",
        });

        // generate new refresh-token
        const rfToken = jwt.sign(userData, JWT_REFRESH_SECRET);

        // update newly generated refresh-token in db
        await LoginAuth.findOneAndUpdate(
            { username: user.username },
            { refresh_token: rfToken }
        ).lean();

        // send access-token in header & refresh-token in body
        res.status(200).json({
            status: "success",
            access_token: accessToken,
            refresh_token: rfToken,
        });

        console.log("New access-token generated!");
    });
};

// Get data use status
const getDataUseStatus = async (req, res) => {
    try {
        let loggedInUser = await validateLoggedInUser(
            req.headers.authorization
        );
        const _user = await LoginDetails.findOne({
            username: loggedInUser.data,
        });

        // console.log(_user);
        if (!_user) {
            return res
                .status(404)
                .json({ status: "error", message: "User not found." });
        }

        const dataUseResponse = await LoginDetails.findById({
            _id: _user._id,
        });

        let dataUseStatus = dataUseResponse.data_use;

        if (dataUseResponse) {
            res.status(200).json({
                status: "success",
                data_status: dataUseStatus,
            });
        } else {
            res.status(400).json({
                status: "error",
                message: "Failed to fetch data_use status",
            });
        }
    } catch (error) {
        return res.status(400).json({ status: "error", message: error });
    }
};

// Update data-use status
const updateDataUseStatus = async (req, res) => {
    try {
        let loggedInUser = await validateLoggedInUser(
            req.headers.authorization
        );
        const _user = await LoginDetails.findOne({
            username: loggedInUser.data,
        });

        // console.log(_user);
        if (!_user) {
            return res
                .status(404)
                .json({ status: "error", message: "User not found." });
        }

        let dataObj = {
            data_use: true,
        };
        let dataUseResponse = await LoginDetails.findByIdAndUpdate(
            _user._id,
            dataObj,
            { new: true }
        );

        if (dataUseResponse) {
            res.status(200).json({
                status: "success",
                message: "Data-use status updated",
                current_status: dataUseResponse.data_use,
            });
        } else {
            res.status(400).json({
                status: "error",
                message: "Failed to update data_use status",
            });
        }
    } catch (error) {
        return res.status(400).json({ status: "error", message: error });
    }
};

module.exports = {
    userRegister,
    userLogin,
    getAllUsers,
    getUser,
    updateUserInfo,
    getAccessToken,
    getDataUseStatus,
    updateDataUseStatus,
};
