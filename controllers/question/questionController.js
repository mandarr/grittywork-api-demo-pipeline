const express = require("express");
require("dotenv").config;

const Question = require("../../models/questions/question");
const Survey = require("../../models/survey/survey");
const QuestionOption = require("../../models/question-option/question-option");

const LoginDetails = require("../../models/login/login-details");

// Services
const validateLoggedInUser = require("../../utils/validateLoggedInUser");

const getAllQuestions = async (req, res) => {
    console.log(req.query);
    try {
        const id = req.query.surveyId;
        const survey = await Survey.findOne({ _id: id }).lean();
        let questions;
        console.log("IS PULSED STATUS", survey.is_pulsed_active, survey);
        if (survey.is_pulsed_active) {
            questions = await Question.find({
                survey_id: id,
            }).lean();
        } else {
            questions = await Question.find({
                survey_id: id,
                is_pulsed: false,
            }).lean();
        }
        const options = await QuestionOption.find({ survey_id: id }).lean();

        // console.log("Questions", "Options", options);

        let _questions = questions.map((item) => {
            let filteredOpt = [];

            if (item.question_type !== "slider_group") {
                filteredOpt = options
                    .filter(
                        (element) =>
                            element.question_id.toString() ===
                            item._id.toString()
                    )
                    .sort(function (a, b) {
                        return a.order - b.order;
                    });

                filteredOpt = filteredOpt.filter((item) => item.is_active == true);
                return { ...item, options: filteredOpt };
            } else {
                filteredOpt = questions
                    .filter(
                        (element) =>
                            element.parent_question_id.toString() ===
                            item._id.toString()
                    )
                    .sort(function (a, b) {
                        return a.order - b.order;
                    });
                filteredOpt = filteredOpt.filter((item) => item.is_active == true);
                console.log('FilteredOpt', filteredOpt);
                return { ...item, options: filteredOpt };
            }
        });

        // console.log(filteredQuestions);
        let filteredQuestions = _questions.filter((item) => {
            return item.question_type !== "slider";
        });
       
        // const points = [40, 100, 1, 5, 25, 10];
        filteredQuestions.sort(function (a, b) {
            return a.order - b.order;
        });
        let isActiveQuestions = filteredQuestions.filter((item) => {
            return item.is_active == true;
        })
        // console.log(filteredQuestions);

        //  filteredQuestions.sort((a,b) =>  return bn )
        res.status(200).json({
            status: "success",
            list: isActiveQuestions,
        });
    } catch (error) {
        res.status(400).json({
            status: "False",
            message: "error",
        });
    }
};

const createQuestion = async (req, res) => {
    try {
        const {
            surveyId,
            question_title,
            question_type,
            ans_code,
            is_active,
            is_mandatory,
            is_pulsed,
            is_numeric,
            min_value,
            max_value,
            keyboard_type,
            options,
            placeholder,
            caption,
            order,
            isSelected,
            min_text,
            max_text,
        } = req.body;
        let loggedInUser = await validateLoggedInUser(
            req.headers.authorization
        );

        if (surveyId && question_title && question_type && ans_code) {
            const _survey = await Survey.findOne({ _id: surveyId });
            if (!_survey) {
                return res
                    .status(404)
                    .json({ status: "error", message: "Survey not found." });
            }

            const _user = await LoginDetails.findOne({
                username: loggedInUser.data,
            });
            if (!_user) {
                return res
                    .status(404)
                    .json({ status: "error", message: "User not found." });
            }

            switch (question_type) {
                case "text":
                    const textQuestion = await Question.create({
                        question_title,
                        question_type,
                        survey_id: _survey._id,
                        ans_code,
                        order,
                        created_at: Date.now(),
                        created_by: _user._id,
                        modified_at: 0,
                        modified_by: "null",
                        is_active,
                        is_pulsed,
                        is_mandatory,
                        is_numeric,
                        min_value,
                        max_value,
                        keyboard_type,
                        parent_question_id: "null",
                        caption,
                        isSelected: false,
                        min_text: "",
                        max_text: "",
                        placeholder,
                    });

                    // const option = await QuestionOption.create({
                    //     option_text: text,
                    //     option_value: value,
                    //     question_id: textQuestion._id,
                    //     created_at: Date.now(),
                    //     created_by: _user._id,
                    //     modified_at: 0,
                    //     modified_by: "null",
                    //     is_active,
                    // });

                    // console.log(option);

                    res.status(200).json({
                        status: "success",
                        message: `${question_type} question successfully added`,
                        question: textQuestion,
                    });

                    break;

                case "checkbox":
                    const checkboxQuestion = await Question.create({
                        question_title,
                        question_type,
                        survey_id: _survey._id,
                        ans_code,
                        order,
                        created_at: Date.now(),
                        created_by: _user._id,
                        modified_at: 0,
                        modified_by: "null",
                        is_active,
                        is_pulsed,
                        is_mandatory,
                        is_numeric: false,
                        min_value: 0,
                        max_value: 0,
                        keyboard_type: "default",
                        parent_question_id: "null",
                        caption: "",
                        isSelected: false,
                        min_text: "",
                        max_text: "",
                        placeholder: "",
                    });
                    options.forEach(async (element) => {
                        await QuestionOption.create({
                            option_text: element.text,
                            option_value: element.value,
                            order: element.order,
                            question_id: checkboxQuestion._id.toString(),
                            created_at: Date.now(),
                            created_by: _user._id,
                            isSelected: false,
                            modified_at: 0,
                            image: element.image,
                            modified_by: "null",
                            is_active,
                            image: element.image,
                        });
                    });

                    res.status(200).json({
                        status: "success",
                        message: `${question_type} question successfully added`,
                    });

                    break;

                case "radio":
                    const radioQuestion = await Question.create({
                        question_title,
                        question_type,
                        survey_id: _survey._id,
                        ans_code,
                        order,
                        created_at: Date.now(),
                        created_by: _user._id,
                        modified_at: 0,
                        modified_by: "null",
                        is_active,
                        is_pulsed,
                        is_mandatory,
                        is_numeric: false,
                        min_value: 0,
                        max_value: 0,
                        keyboard_type: "default",
                        parent_question_id: "null",
                        caption: "",
                        isSelected: true,
                        min_text: "",
                        max_text: "",
                        placeholder: "",
                    });
                    options.forEach(async (element) => {
                        await QuestionOption.create({
                            option_text: element.text,
                            option_value: element.value,
                            order: element.order,
                            question_id: radioQuestion._id.toString(),
                            created_at: Date.now(),
                            created_by: _user._id,
                            isSelected: false,
                            modified_at: 0,
                            image: element.image,
                            modified_by: "null",
                            image: element.image,
                            is_active,
                        });
                    });

                    res.status(200).json({
                        status: "success",
                        message: `${question_type} question successfully added`,
                    });

                    break;

                case "slider_group":
                    const sliderGroupQuestion = await Question.create({
                        question_title,
                        question_type,
                        order,
                        survey_id: _survey._id,
                        ans_code: ans_code,
                        created_at: Date.now(),
                        created_by: _user._id,
                        modified_at: 0,
                        modified_by: "null",
                        is_active,
                        is_pulsed,
                        is_mandatory,
                        is_numeric: false,
                        min_value: 0,
                        max_value: 0,
                        keyboard_type: "default",
                        parent_question_id: "null",
                        caption: "",
                        isSelected: false,
                        min_text: "",
                        max_text: "",
                        placeholder: "",
                    });

                    options.forEach(async (element) => {
                        await Question.create({
                            question_title: element.text,
                            question_type: "slider",
                            survey_id: _survey._id,
                            order: element.order,
                            ans_code: element.ans_code,
                            created_at: Date.now(),
                            created_by: _user._id,
                            modified_at: 0,
                            modified_by: "null",
                            is_active: element.is_active,
                            is_pulsed: element.is_pulsed,
                            is_mandatory: element.is_mandatory,
                            is_numeric: false,
                            min_value: element.min_value,
                            max_value: element.max_value,
                            keyboard_type: "default",
                            parent_question_id: sliderGroupQuestion._id,
                            caption: "",
                            placeholder: "",
                            isSelected: false,
                            min_text: element.min_text,
                            max_text: element.max_text,
                        });
                    });

                    res.status(200).json({
                        status: "success",
                        message: `${question_type} question successfully added`,
                    });

                    break;

                case "tab":
                    const tabQuestion = await Question.create({
                        question_title,
                        question_type,
                        survey_id: _survey._id,
                        ans_code,
                        order,
                        created_at: Date.now(),
                        created_by: _user._id,
                        modified_at: 0,
                        modified_by: "null",
                        is_active,
                        is_pulsed,
                        is_mandatory,
                        is_numeric: false,
                        min_value: 0,
                        max_value: 0,
                        keyboard_type: "default",
                        parent_question_id: "null",
                        placeholder: "",
                        caption: "",
                        isSelected: false,
                        min_text: "",
                        max_text: "",
                    });
                    options.forEach(async (element) => {
                        await QuestionOption.create({
                            option_text: element.text,
                            option_value: element.value,
                            order: element.order,
                            question_id: tabQuestion._id,
                            created_at: Date.now(),
                            created_by: _user._id,
                            modified_at: 0,
                            modified_by: "null",
                            is_active,
                            isSelected: false,
                            image: element.image,
                        });
                    });
                    res.status(200).json({
                        status: "success",
                        message: `${question_type} question successfully added`,
                        question: tabQuestion,
                    });
                    break;

                case "calendar":
                    const calendarQuestion = await Question.create({
                        question_title,
                        question_type,
                        survey_id: _survey._id,
                        ans_code,
                        order,
                        created_at: Date.now(),
                        created_by: _user._id,
                        modified_at: 0,
                        modified_by: "null",
                        is_active,
                        is_pulsed,
                        is_mandatory,
                        is_numeric: false,
                        min_value: 0,
                        max_value: 0,
                        keyboard_type: "default",
                        parent_question_id: "null",
                        caption: "",
                        isSelected: true,
                        min_text: "",
                        max_text: "",
                        placeholder: "",
                    });

                    res.status(200).json({
                        status: "success",
                        message: `${question_type} question successfully added`,
                        question: calendarQuestion,
                    });

                    break;

                case "textarea":
                    const textAreaQuestion = await Question.create({
                        question_title,
                        question_type,
                        survey_id: _survey._id,
                        ans_code,
                        order,
                        created_at: Date.now(),
                        created_by: _user._id,
                        modified_at: 0,
                        modified_by: "null",
                        is_active,
                        is_pulsed,
                        is_mandatory,
                        is_numeric: false,
                        min_value: 0,
                        max_value: 0,
                        keyboard_type,
                        parent_question_id: "null",
                        placeholder,
                        caption,
                        isSelected: false,
                        min_text: "",
                        max_text: "",
                    });

                    res.status(200).json({
                        status: "success",
                        message: `${question_type} question successfully added`,
                        question: textAreaQuestion,
                    });
                    break;

                default:
                    res.status(400).json({
                        status: "error",
                        message: "Enter correct data",
                    });
                    break;
            }
        } else {
            res.status(400).json({
                status: "error",
                message: "Incomplete data provided.",
            });
        }
    } catch (error) {
        console.log("Error: ", error);
        res.status(400).json({
            status: "error",
            message: "Failed to add question",
        });
    }
};

const getQuestion = async (req, res) => {
    try {
        const user = await Questions.findById({ _id: req.query.questionId });
        res.status(200).json({ status: "Success", user: user });
    } catch (error) {
        return res.status(400).json({
            status: "False",
            message: "Not found",
        });
    }
};

const updateQuestion = async (req, res) => {
    try {
        const questionId = req.query.questionId;
        const { survey_name, question_title, question_type, is_active } =
            req.body;
        const surveyName = await Survey.findOne({ survey_name });
        const result = await Question.findById(questionId);

        if (!surveyName) {
            return res.status(404).json({
                status: "error",
                message: "Survey Name not found",
            });
        }

        const data = {
            question_title,
            question_type,
            modified_at: Date.now(),
            modified_by: surveyName._id,
            is_active: is_active === undefined ? result.is_active : is_active,
        };
        await Question.findByIdAndUpdate(questionId, data);
        res.status(200).json({
            status: "success",
            message: "Question updated successfully",
        });
    } catch (error) {
        res.status(400).json({
            status: "error",
            message: "Failed to update question",
        });
    }
};

module.exports = {
    createQuestion,
    getAllQuestions,
    getQuestion,
    updateQuestion,
};
