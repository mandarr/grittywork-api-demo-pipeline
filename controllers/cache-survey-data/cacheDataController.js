const express = require("express");

require("dotenv").config;

// Service
const validateLoggedInUser = require("../../utils/validateLoggedInUser");

// Survey model
const Survey = require("../../models/survey/survey");

// Login-details model
const LoginDetails = require("../../models/login/login-details");

const CachedData = require("../../models/cached-survey-data/cached-survey-data");

const postData = async (req, res) => {
    try {
        const { list, surveyId, currentQuiz } = req.body;
        let loggedInUser = await validateLoggedInUser(
            req.headers.authorization
        );

        if (list && surveyId) {
            const user = await LoginDetails.findOne({
                username: loggedInUser.data,
            });

            if (!user) {
                return res
                    .status(404)
                    .json({ status: "error", message: "User not found." });
            }

            const data = await CachedData.create({
                list,
                user_id: user._id,
                survey_id: surveyId,
                date: Date.now(),
                currentQuiz,
            });

            console.log(data);
            return res.status(200).json({
                status: "success",
                dataId: data._id,
            });
        } else {
            return res.status(400).json({
                status: "error",
                message: "Please provide list/surveyId/username/date.",
            });
        }
    } catch (error) {
        return res
            .status(400)
            .json({ status: "error", message: "Error occured." });
    }
};

const getData = async (req, res) => {
    try {
        const { username, surveyId } = req.query;
        let loggedInUser = await validateLoggedInUser(
            req.headers.authorization
        );

        const _user = await LoginDetails.findOne({
            username: loggedInUser.data,
        });

        // console.log(_user);
        if (!_user) {
            return res
                .status(404)
                .json({ status: "error", message: "User not found." });
        }

        const data = await CachedData.findOne({
            user_id: _user._id,
            survey_id: surveyId,
        });

        console.log(data);
        let surveyList = data.list;
        return res
            .status(200)
            .json({
                status: "success",
                list: surveyList,
                dataId: data._id,
                date: data.date,
            });
    } catch (error) {
        return res
            .status(400)
            .json({ status: "error", message: "Error occured." });
    }
};

const updateData = async (req, res) => {
    try {
        const { dataId, list, date, currentQuiz } = req.body;
        if (dataId && list && date) {
            const data = await CachedData.findByIdAndUpdate(dataId, {
                list,
                date,
                currentQuiz,
            });

            return res
                .status(200)
                .json({ status: "success", message: "Cache updated." });
        } else {
            return res.status(400).json({
                status: "error",
                message: "Please provide list/dataId/date.",
            });
        }
    } catch (error) {
        return res
            .status(400)
            .json({ status: "error", message: "Error occured." });
    }
};

const removeData = async (req, res) => {
    try {
        const { dataId } = req.query;
        if (dataId) {
            const data = await CachedData.findByIdAndRemove(dataId);

            return res
                .status(200)
                .json({ status: "success", message: "Cache cleared." });
        } else {
            return res.status(400).json({
                status: "error",
                message: "Please provide list/surveyId/username/date.",
            });
        }
    } catch (error) {
        return res
            .status(400)
            .json({ status: "error", message: "Error occured." });
    }
};

module.exports = {
    postData,
    getData,
    updateData,
    removeData,
};
