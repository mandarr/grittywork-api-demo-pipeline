const express = require("express");
require("dotenv").config;

const Questions = require("../../models/questions/question");

const Survey = require("../../models/survey/survey");

const QuestionOption = require("../../models/question-option/question-option");

const getOptionData = async (req, res) => {
    try {
        const questionOption = await QuestionOption.find();
        res.status(200).json({
            status: "success",
            list: questionOption,
        });
        console.log(questionOption, "question");
    } catch (error) {
        res.status(400).json({
            status: "False",
            message: "error",
        });
    }
};

const createOptionData = async (req, res) => {
    try {
        const {
            option_text,
            option_value,
            survey_name,
            question_title,
            is_active,
        } = req.body;
        if (
            option_text &&
            option_value &&
            survey_name &&
            question_title &&
            is_active
        ) {
            const survey = await Survey.findOne({ survey_name }).lean();
            if (!survey) {
                return res.status(404).json({
                    status: "False",
                    message: "Survey not found",
                });
            }
            const question = await Questions.findOne({ question_title }).lean();
            if (!question) {
                return res.status(404).json({
                    status: "False",
                    message: "Question not found",
                });
            }
            await QuestionOption.create({
                survey_name,
                question_title,
                option_text,
                option_value,
                is_active,
                created_at: Date.now(),
                created_by: question._id,
                modified_at: 0,
                modified_by: "null",
            });
            return res.status(200).json({
                status: "Success",
                message: "Data Added...",
            });
        } else {
            return res.status(400).json({
                status: "Error",
                message: "Incomplete Data",
            });
        }
    } catch (error) {
        return res.status(400).json({
            status: "error",
            message: "Failed to add data.",
            error: error,
        });
    }
};

module.exports = {
    getOptionData,
    createOptionData,
};
