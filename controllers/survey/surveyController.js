const express = require("express");

require("dotenv").config;

const Answer = require("../../models/answers/answer");

// Survey model
const Survey = require("../../models/survey/survey");

// Login-details model
const LoginDetails = require("../../models/login/login-details");

// Services
const validateLoggedInUser = require("../../utils/validateLoggedInUser");

// Get CMS Sureveylist
const getCmsSurveyList = async (req, res) => {
  try {
    let survey = await Survey.find();
    let serveyList = [];

    for (let surveyObj of survey) {
      let ans = await Answer.find({ survey_id: surveyObj._id });
      let obj = surveyObj.toObject();
      obj["ansCount"] = ans.length;
      serveyList.push(obj);
    }

    res.status(200).json({
      status: "success",
      serveyList: serveyList,
    });
  } catch (error) {
    res.status(400).json({
      status: "error",
      message: error.message,
    });
  }
};

// Get all surveys
const getSurveyList = async (req, res) => {
  try {
    const surveys = await Survey.find();
    res.status(200).json({
      status: "success",
      list: surveys,
    });
    // console.log("user", surveys);
  } catch (error) {
    res.status(400).json({ status: "failed", message: "error" });
  }
};

// Create survey
const createSurvey = async (req, res) => {
  try {
    const { survey_name, is_active, is_pulsed_active } = req.body;
    let loggedInUser = await validateLoggedInUser(req.headers.authorization);
    if (survey_name) {
      const user = await LoginDetails.findOne({
        username: loggedInUser.data,
      }).lean();
      if (!user) {
        return res.status(404).json({
          status: "error",
          message: "User not found.",
        });
      }
      await Survey.create({
        survey_name,
        is_active,
        is_pulsed_active,
        created_at: Date.now(),
        created_by: user._id,
        modified_at: 0,
        modified_by: "null",
      });
      return res.status(200).json({
        status: "success",
        message: "Survey created.",
      });
    } else {
      return res.status(400).json({
        status: "error",
        message: "Please provide username/survey_name",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: "error",
      message: "Failed to create survey.",
      error: error,
    });
  }
};

// Get survey
const getSurvey = async (req, res) => {
  try {
    const result = await Survey.findOne({ _id: req.query.surveyId });
    res.status(200).json({ status: "success", data: result });
  } catch (error) {
    res.status(400).json({
      status: "error",
      message: "Failed to fetch survey.",
    });
  }
};

// Update survey
const updateSurvey = async (req, res) => {
  try {
    const surveyId = req.query.surveyId;
    let loggedInUser = await validateLoggedInUser(req.headers.authorization);

    const { survey_name, is_active, is_pulsed_active } = req.body;
    const user = await LoginDetails.findOne({
      username: loggedInUser.data,
    }).lean();
    const result = await Survey.findById(surveyId);
    if (!result) {
      return res
        .status(404)
        .json({ status: "error", message: "Survey not found." });
    }

    if (!user) {
      return res.status(404).json({
        status: "error",
        message: "User not found.",
      });
    }

    const data = {
      survey_name,
      modified_by: user._id,
      modified_at: Date.now(),
      is_active: is_active === undefined ? result.is_active : is_active,
      is_pulsed_active:
        is_pulsed_active === undefined
          ? result.is_pulsed_active
          : is_pulsed_active,
    };
    await Survey.findByIdAndUpdate(surveyId, data);
    res.status(200).json({
      status: "success",
      message: "Survey updated.",
    });
  } catch (error) {
    res.status(400).json({
      status: "error",
      message: "Failed to update survey.",
    });
  }
};

module.exports = {
  getCmsSurveyList,
  getSurveyList,
  createSurvey,
  getSurvey,
  updateSurvey,
};
